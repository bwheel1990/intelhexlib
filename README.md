# Intel Hex Lib

This project is an opensource .net library of useful functions for interacting with Intel Hex records. A link to the specification can be found at [Wikipedia](https://en.wikipedia.org/wiki/Intel_HEX).

## Building
TODO: add build scripts(MSBUILD or dotnet)


## Running Tests
TODO: Add instructions for running the unit tests.


## Documentation
TODO: add documentation and examples.