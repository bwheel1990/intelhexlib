﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntelHexLib
{
    /// <summary>
    /// The Record types as specified by the specification.
    /// <see href="https://en.wikipedia.org/wiki/Intel_HEX">wikipedia</see>
    /// </summary>
    public enum RecordTypes : byte
    {
        Data = 0x00,
        EndOfFile = 0x01,
        ExtendedSegmentAddress = 0x02,
        StartSegmentAddress = 0x03,
        ExtendedLinearAddress = 0x04,
        StartLinearAddress = 0x05
    }
}
