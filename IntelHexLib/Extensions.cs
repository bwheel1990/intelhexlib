﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntelHexLib
{
    public static class Extensions
    {
        public static byte TwosComplementSum(this IEnumerable<byte> data )
        {
            var checksum = data.Aggregate((agg, element) =>
            {
                agg += element;
                agg &= 0xff;
                return agg;
            });
            checksum = (byte)(256 - checksum);
            return checksum;
        }


        public static byte[] HexToBytes(this string text)
        {
            return Enumerable.Range(0, text.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(text.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string BytesToHex(this IEnumerable<byte> data)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var b in data)
                sb.AppendFormat("{0:X2}", b);
            return sb.ToString();
        }
    }
}
