﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IntelHexLib
{
    /// <summary>
    /// This library contains functions for decoding Intel Hex into <see cref="Record"/>s and Encoding <see cref="Record"/>s into Inte Hex.
    /// </summary>
    public static class IntelHex
    {
        /// <summary>
        /// This method will open the file passed in, decode the Intel Hex lines, and close the file.
        /// </summary>
        /// <param name="filepath">The path to the file to decode.</param>
        /// <returns>Multiple Intel Hex <see cref="Record"/>s.</returns>
        public static IEnumerable<Record> Decode(string filepath)
        {
            using (var inputStream = File.OpenRead(filepath))
            {
                return Decode(inputStream);
            }
        }

        /// <summary>
        /// This method is an asynchronous wrapper to <see cref="Decode(string)"/>.
        /// </summary>
        /// <param name="filepath">The file to decode.</param>
        /// <returns>The reference to the task running the <see cref="Decode(string)"/> method.</returns>
        public static async Task<IEnumerable<Record>> DecodeAsync(string filepath)
        {
            return await Task.Run(() => Decode(filepath));
        }

        /// <summary>
        /// This method will read lines from the input stream and decode as Intel Hex format.
        /// </summary>
        /// <param name="stream">The stream to decode.</param>
        /// <returns>Multiple Intel Hex <see cref="Record"/>s.</returns>
        public static IEnumerable<Record> Decode(Stream stream)
        {
            StreamReader sr = new StreamReader(stream);
            while (sr.Peek() > 0)
            {
                var line = sr.ReadLine();
                yield return DecodeLine(line);
            }
        }

        /// <summary>
        /// This method is an asynchronous wrapper to <see cref="Decode(Stream)"/>.
        /// </summary>
        /// <param name="stream">The stream to decode.</param>
        /// <returns>The task which is running <see cref="Decode(Stream)"/>.</returns>
        public static async Task<IEnumerable<Record>> DecodeAsync(Stream stream)
        {
            return await Task.Run(() => Decode(stream));
        }

        /// <summary>
        /// This method will decode the collection of lines according to the Intel Hex format.
        /// </summary>
        /// <param name="lines">The lines to decode.</param>
        /// <returns>Multiple Intel Hex <see cref="Record"/>s.</returns>
        public static IEnumerable<Record> Decode(IEnumerable<string> lines)
        {
            foreach (var line in lines )
            {
                yield return DecodeLine(line);
            }
        }

        /// <summary>
        /// This method is an asynchronous version of <see cref="Decode(IEnumerable{string})"/>.
        /// </summary>
        /// <param name="lines">the lines of hexadecimal to Decode.</param>
        /// <returns>The reference to the <see cref="Decode(IEnumerable{string})"/> task.</returns>
        public static async Task<IEnumerable<Record>> DecodeAsync(IEnumerable<string> lines)
        {
            return await Task.Run(() => Decode(lines));
        }

        /// <summary>
        /// This method will decode a single line according to the Intel Hex format.
        /// </summary>
        /// <param name="line">A hexadecimal line.</param>
        /// <returns>A single Intel Hex <see cref="Record"/>, or null if it couldn't parse it. </returns>
        public static Record DecodeLine(string line)
        {
            // remove the leading Colon.
            line = line.Trim().Trim(':'); // remove the first character.

            // get the byte count.
            var byteCountHex = line.Substring(0, 2);
            var byteCount = Convert.ToByte(byteCountHex, 16);

            // get the address.
            var addressHex = line.Substring(2, 4);
            var address = Convert.ToUInt16(addressHex, 16);

            // get the record type
            var recordTypeHex = line.Substring(6, 2);
            var recordTypeByte = Convert.ToByte(recordTypeHex, 16);
            var recordType = (RecordTypes)recordTypeByte;

            // get the data.
            var dataHex = line.Substring(8, byteCount*2);
            var data = dataHex.HexToBytes();

            // get the checksum
            var checkSumHex = line.Substring(8 + (byteCount*2), 2);

            // calculate the checksum
            var allButCheckSumHex = line.Substring(0, line.Length - 2);
            var allButCheckSum = allButCheckSumHex.HexToBytes();
            var calculatedChecksum = allButCheckSum.TwosComplementSum();
            var calculatedChecksumHex = calculatedChecksum.ToString("X2");

            // check if it has the proper checksum
            if (checkSumHex == calculatedChecksumHex)
            {
                return new Record()
                {
                    ByteCount = byteCount,
                    Address = address,
                    RecordType = recordType,
                    Data = data,
                    Checksum = calculatedChecksum
                };
            }
            return null;
        }

        /// <summary>
        /// The asynchronous version of <see cref="DecodeLine(string)"/>.
        /// </summary>
        /// <param name="line">The line to decode.</param>
        /// <returns>The reference to the <see cref="DecodeLine(string)"/> task.</returns>
        public static async Task<Record> DecodeLineAsync(string line)
        {
            return await Task.Run(() => DecodeLine(line));
        }

        /// <summary>
        /// This method will Encode the <see cref="Record"/>s passed in, and put them in the file located at the filepath passed in. 
        /// </summary>
        /// <param name="records">The <see cref="Record"/>s to encode.</param>
        /// <param name="filepath">The file location to put the encoded <see cref="Record"/>s.</param>
        public static void Encode(IEnumerable<Record> records, string filepath)
        {
            using (var stream = File.OpenWrite(filepath))
            {
                Encode(records, stream);
            }
        }

        /// <summary>
        /// This method is an asynchronous wrapper to <see cref="Encode(IEnumerable{Record}, string)"/>.
        /// </summary>
        /// <param name="records">The collection of <see cref="Record"/>s to encode.</param>
        /// <param name="filepath">The location of the file to put them.</param>
        /// <returns>The task running the <see cref="Encode(IEnumerable{Record}, string)"/> method.</returns>
        public static async Task EncodeAsync(IEnumerable<Record> records, string filepath)
        {
            await Task.Run(() => Encode(records, filepath));
        }

        /// <summary>
        /// This method will write to the stream passed in all the <see cref="Record"/>s that are passed in as well.
        /// </summary>
        /// <param name="records">The <see cref="Record"/>s to encode.</param>
        /// <param name="stream">The file location to put the encoded <see cref="Record"/>s</param>
        public static void Encode(IEnumerable<Record> records, Stream stream)
        {
            StreamWriter writer = new StreamWriter(stream);
            foreach (var record in records)
            {
                writer.WriteLine(Encode(record));
            }
        }

        /// <summary>
        /// This method is an asynchronous wrapper to <see cref="Encode(IEnumerable{Record}, Stream)"/>.
        /// </summary>
        /// <param name="records">The colleciton of <see cref="Record"/>s to encode.</param>
        /// <param name="stream">The stream to put the <see cref="Record"/>s.</param>
        /// <returns></returns>
        public static async Task EncodeAsync(IEnumerable<Record> records, Stream stream)
        {
            await Task.Run(() => Encode(records, stream));
        }

        /// <summary>
        /// This method encodes a collection of <see cref="Record"/>s into Intel Hexl
        /// </summary>
        /// <param name="records">Records to encode.</param>
        /// <returns>The records encoded as Intel Hex.</returns>
        public static IEnumerable<string> Encode(IEnumerable<Record> records)
        {
            foreach (var record in records)
                yield return Encode(record);
        }

        /// <summary>
        /// This method is an asynchronous wrapper to <see cref="Encode(IEnumerable{Record})"/>.
        /// </summary>
        /// <param name="records">A collection of <see cref="Record"/>s to encode.</param>
        /// <returns>The records as a collection of strings formatted according to the Intel Hex specification.</returns>
        public static async Task<IEnumerable<string>> EncodeAsync(IEnumerable<Record> records)
        {
            return await Task.Run(() => Encode(records));
        }

        /// <summary>
        /// This method encodes a <see cref="Record"/> as a line of Intel Hex. It assumes the <see cref="Record"/> is fully 
        /// populated, and the checksum is valid.
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public static string Encode(Record record)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(":");
            sb.Append(record.ByteCount.ToString("X2"));
            sb.Append(record.Address.ToString("X4"));
            sb.Append(((byte)(record.RecordType)).ToString("X2"));
            sb.Append(record.Data.BytesToHex());
            sb.Append(record.Checksum);
            return sb.ToString();
        }

        /// <summary>
        /// This method is an asynchronous wrapper to <see cref="Encode(Record)"/>.
        /// </summary>
        /// <param name="record">The <see cref="Record"/> to encode.</param>
        /// <returns>The <see cref="Record"/> passed in encoded to the Intel Hex format.</returns>
        public static async Task<string> EncodeAsync(Record record)
        {
            return await Task.Run(() => Encode(record));
        }
    }
}
