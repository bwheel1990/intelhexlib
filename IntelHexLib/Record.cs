﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntelHexLib
{
    /// <summary>
    /// This data structure holds a single line from a Intel Hex File.
    /// <see href="https://en.wikipedia.org/wiki/Intel_HEX"/>
    /// </summary>
    public class Record
    {
        /// <summary>
        /// This property holds the Byte count of the record. 
        /// </summary>
        public byte ByteCount { get; set; }

        /// <summary>
        /// This property holds the address of the record. 
        /// </summary>
        public ushort Address { get; set; }

        /// <summary>
        /// This property holds the type of the record according to the specification.
        /// </summary>
        public RecordTypes RecordType { get; set; }

        /// <summary>
        /// This property holds the data.
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// This property holds the checksum.
        /// </summary>
        public byte Checksum { get; set; }

        /// <summary>
        /// This method will print out the record with the different fields parsed out into hexadecimal format.
        /// </summary>
        /// <returns>The <see cref="Record"/> in hexadecimal.</returns>
        public override string ToString()
        {
            var byteCountStr = ByteCount.ToString("X2").Replace("-","");
            var addressStr = Address.ToString("X4").Replace("-", "");
            var recordTypeStr = RecordType.ToString();
            var dataStr = Data.BytesToHex().Replace("-", "");
            var checkSumStr = Checksum.ToString("X2").Replace("-", "");
            return $"ByteCount: {byteCountStr}, Address: {addressStr}, RecordType: {recordTypeStr}, Data: {dataStr}, Checksum: {checkSumStr}";
        }

        /// <summary>
        /// Thie method test equality based on the contents of the <see cref="Record"/>'s properties.
        /// </summary>
        /// <param name="obj">The other <see cref="Record"/> to test.</param>
        /// <returns>True = they are equal. False, they are not equal.</returns>
        public override bool Equals(object obj)
        {
            Record r = obj as Record;
            return r?.ByteCount == ByteCount &&
                r?.Address == Address &&
                r?.RecordType == RecordType &&
                Enumerable.SequenceEqual(r?.Data, Data) &&
                r?.Checksum == Checksum;
        }

        /// <summary>
        /// This method is implemented for useing <see cref="Record"/> in Dictionaries.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + ByteCount.GetHashCode();
                hash = hash * 23 + Address.GetHashCode();
                hash = hash * 23 + RecordType.GetHashCode();
                hash = hash * Data.GetHashCode();
                hash = hash * Checksum.GetHashCode();
                return hash;
            }
        }
    }
}
