﻿using System;
using IntelHexLib;
namespace IntelHexLib.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            var hexLines = @"
:1003200080937A0080917A00826080937A00809135
:100330007A00816080937A0080917A00806880934F
:100340007A001092C100EDE9F0E02491E9E8F0E0D4
:100350008491882399F090E0880F991FFC01E85957
:10036000FF4FA591B491FC01EE58FF4F85919491F8
:100370008FB7F894EC91E22BEC938FBFC0E0D0E004
:1003800081E00E9470000E94DE0080E00E94700008
:100390000E94DE002097A1F30E940000F1CFF894A4
:0203A000FFCF8D
:107E0000112484B714BE81FFF0D085E080938100F7
:107E100082E08093C00088E18093C10086E0809377
:107E2000C20080E18093C4008EE0C9D0259A86E02C
:107E300020E33CEF91E0309385002093840096BBD3
:107E4000B09BFECF1D9AA8958150A9F7CC24DD24C4
:107E500088248394B5E0AB2EA1E19A2EF3E0BF2EE7
:107E6000A2D0813461F49FD0082FAFD0023811F036
:107E7000013811F484E001C083E08DD089C08234E0";

            Console.WriteLine($"IntelHex: two's complement: {hexLines}");
            foreach (var record in IntelHex.Decode(hexLines.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries)))
            {
                if (record == null)
                    Console.Out.WriteLine("Null");
                else
                    Console.Out.WriteLine(record.ToString());
            }
            
            Console.Out.WriteLine("Press enter to quit.");
            Console.In.ReadLine();
        }
    }
}
